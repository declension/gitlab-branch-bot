SHELL:=/bin/bash
URL = $(shell poetry run chalice url)
.PHONY: clean ping build

deploy: requirements.txt 
	poetry run chalice deploy --stage ci --no-autogen-policy

clean:
	rm requirements.txt; poetry install

requirements.txt: clean
	poetry export -f requirements.txt -o requirements.txt

build:
	poetry install

ping: 
	http -v $(URL)slack/events type="event_callback" 'event:={"type": "message", "user": "123456", "channel": "GSYTLEA2J", "text": "Hello, world!"}'
