from chalice import Chalice
from slack import WebClient

from chalicelib import NICE_LOG_FORMAT
from chalicelib.gitlab import GitlabClient


class SecretiveChalice(Chalice):
    """Like normal, but:
        * Takes custom dependencies
        * Nicer log format"""
    FORMAT_STRING = NICE_LOG_FORMAT

    def __init__(self, app_name,
                 slack_client: WebClient,
                 gitlab_client: GitlabClient,
                 debug=True, configure_logs=True, env=None):
        super().__init__(app_name, debug=debug, configure_logs=configure_logs,
                         env=env)
        self.gitlab_client = gitlab_client
        self.slack_client = slack_client
        self.log.info("Started up %s", type(self).__name__)
