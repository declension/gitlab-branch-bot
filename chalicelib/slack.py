import asyncio
from datetime import datetime, timedelta
from enum import Enum
from html import escape, unescape
from logging import getLogger
from ssl import SSLContext
from typing import Text, Any, Optional, Iterable, Tuple, Dict

import aiohttp
from dateutil.tz import UTC
from slack import WebClient
from slack.web import get_user_agent

from chalicelib.aws import ConfigStore
from chalicelib.domain import MergeRequest, MergeStatus, Project, Person

MERGE_STATUS_LOOKUP = {MergeStatus.CANNOT_BE_MERGED: ":x:",
                       MergeStatus.CAN_BE_MERGED: ":heavy_check_mark:",
                       MergeStatus.UNHANDLED: ":grey_question:"}

Token = Text
Markup = Text
logger = getLogger(__name__)


def sanitise(s: Text) -> Markup:
    return escape(unescape(s), False)


def multiple_emoji(code: Markup, n: int) -> Markup:
    """Returns some badge-like things in a nice way"""
    if n <= 0:
        return ""
    return f" ({code})" if n == 1 else f" ({code}×{n})"


def format_mrs(mr: MergeRequest) -> Markup:
    text = (f"_<{sanitise(mr.link.url)}|{sanitise(mr.title)}>_"
            f" by {sanitise(mr.author.name)}")
    text += multiple_emoji(":thumbsup:", mr.upvotes)
    text += multiple_emoji(":thumbsdown:", mr.downvotes)

    if datetime.now(tz=UTC) - mr.created_at > timedelta(days=3):
        text = f"{text} :alarm_clock:"
    return f" ‣ {MERGE_STATUS_LOOKUP.get(mr.merge_status)} {text}"


def format_project(project: Project) -> Markup:
    num = len(project.mrs)
    header = f"{num} in <{project.url}|{project.name}>:"
    mrs_body = "\n".join(format_mrs(mr) for mr in project.mrs)
    return f"\n{header}\n{mrs_body}"


def format_people_freqs(people: Iterable[Tuple[Person, int]]) -> Markup:
    def format_person(p: Optional[Person]) -> Text:
        return f"<{sanitise(p.url.url)} | {sanitise(p.name)}>" if p else "_nobody_"

    return ", ".join(f"{format_person(p)} ({n})"
                     for p, n in people)


class HandledEvent(Enum):
    MESSAGE = "message"
    APP_MENTION = "app_mention"


class LazySlackClient(WebClient):
    """Lazily gets the token via a ConfigStore"""
    _INSTANCE = None

    def __new__(cls, *args, **kwargs) -> Any:
        if not cls._INSTANCE:
            cls._INSTANCE = super().__new__(cls)
        return cls._INSTANCE

    def __init__(
        self,
        config: ConfigStore,
        token: Optional[str] = None,
        base_url: str = WebClient.BASE_URL,
        timeout: int = 10,
        loop: Optional[asyncio.AbstractEventLoop] = None,
        ssl: Optional[SSLContext] = None,
        proxy: Optional[str] = None,
        run_async: bool = False,
        use_sync_aiohttp: bool = False,
        session: Optional[aiohttp.ClientSession] = None,
        headers: Optional[Dict] = None,
        user_agent_prefix: Optional[str] = None,
        user_agent_suffix: Optional[str] = None,
    ):
        # Can't call super().__init__, as we're overriding `.token`
        self.base_url = base_url
        self.timeout = timeout
        self.ssl = ssl
        self.proxy = proxy
        self.run_async = run_async
        self.session = session
        self.headers = headers or {}
        self._logger = getLogger(__name__)
        self._event_loop = loop
        self.use_sync_aiohttp = use_sync_aiohttp
        self.session = session
        self.headers = headers or {}
        self.headers["User-Agent"] = get_user_agent(
            user_agent_prefix, user_agent_suffix
        )
        self._logger = getLogger(__name__)
        self._event_loop = loop

        # Overriden / extra
        self._token = None
        self.config = config
        self._logger.info("Created new %s (to %s)", type(self).__name__, self.base_url)

    @property
    def token(self) -> Token:
        """Lazy, cached token access"""
        if not self._token:
            self._token = self.config.slack_api_token
        return self._token
