from collections import Counter
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import Text, Iterable, Sequence, Tuple, Optional, Callable, Set

from urllib3.util import Url

ProjectId = int


class MergeStatus(Enum):
    CAN_BE_MERGED = "can_be_merged"
    CANNOT_BE_MERGED = "cannot_be_merged"
    UNHANDLED = None


@dataclass(frozen=True)
class Person:
    name: Text
    url: Url
    avatar_url: Optional[Url]


@dataclass(frozen=True)
class MergeRequest:
    title: Text
    project_id: ProjectId
    link: Url
    author: Person
    assignee: Optional[Person]
    created_at: datetime
    updated_at: datetime
    merge_status: MergeStatus
    upvotes: int
    downvotes: int
    wip: bool


@dataclass(frozen=True)
class Project:
    id: ProjectId
    name: Text
    url: Url
    mrs: Set[MergeRequest]
    """All relevant Merge Requests in this project.
    Mutable for collection algorithm to add as it goes"""


def busiest_people(mapper: Callable[[MergeRequest], Optional[Person]],
                   projects: Iterable[Project], max=2) -> Sequence[Tuple[Optional[Person], int]]:
    """Gets the n authors with the highest number of MRs open (over 1)"""
    authors = [mapper(mr) for p in projects for mr in p.mrs]
    return [e
            for e in Counter(authors).most_common(max)
            if e[1] > 1]
