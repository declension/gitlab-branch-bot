import json
from collections import defaultdict
from datetime import datetime
from functools import lru_cache
from logging import getLogger, basicConfig, INFO
from typing import Dict, Text, Iterable, Any, Optional
from urllib.parse import urljoin
from urllib.request import Request, urlopen

from aiohttp import ClientTimeout
from urllib3.util import Url, parse_url

from chalicelib.aws import ConfigStore, EnvConfigStore
from chalicelib.domain import MergeRequest, ProjectId, MergeStatus, Project, Person

logger = getLogger(__name__)

ProjectsById = Dict[ProjectId, Project]


class GitlabClient:
    def __init__(self, config: ConfigStore):
        self.config = config
        self.timeout = ClientTimeout(total=5)

    def fetch_projects(self) -> ProjectsById:
        """Gets all the MRs for a group, and parses to MergeRequests"""
        req = Request(self.url.url,
                      headers={"authorization": f"Bearer {self.config.gitlab_api_token}"})
        with urlopen(req, timeout=5) as response:
            assert response.getcode() == 200, f"Got HTTP " \
                                              f"{response.getcode()}: " \
                                              f"{response.read()}" \
                                              f" for {self.url}"
            data = json.load(response)
        logger.info("Got Gitlab MRs data: %s", data)
        return self._transform_response(data)

    @staticmethod
    def _transform_response(mrs_data: Iterable[Dict]) -> ProjectsById:

        by_project = defaultdict()
        for data in mrs_data:
            pid = data["project_id"]
            mr = GitlabClient._to_mr(data)
            web_url = parse_url(data["web_url"])
            try:
                by_project[pid].mrs.add(mr)
                logger.info("Adding MR %s to %s", mr, pid)
            except KeyError:
                base_url = parse_url(urljoin(web_url.url, "../.."))
                name = base_url.path.strip('/')
                project = Project(id=pid, name=name, url=base_url, mrs={mr})
                by_project[pid] = project
                logger.info("Adding %s to %s", project, pid)
        return by_project

    @staticmethod
    def _to_mr(data: Dict) -> MergeRequest:
        def datetime_from(s: Text) -> datetime:
            return datetime.fromisoformat(s.replace("Z", "+00:00"))

        created = datetime_from(data["created_at"])
        updated = datetime_from(data["updated_at"])
        return MergeRequest(title=data["title"],
                            link=parse_url(data["web_url"]),
                            project_id=data["project_id"],
                            author=GitlabClient._to_person(data["author"]),
                            assignee=GitlabClient._to_person(data["assignee"]),
                            created_at=created,
                            updated_at=updated,
                            merge_status=GitlabClient.merge_status_for(data),
                            wip=data["work_in_progress"],
                            upvotes=data["upvotes"],
                            downvotes=data["downvotes"])

    @staticmethod
    def merge_status_for(data):
        try:
            return MergeStatus(data["merge_status"])
        except (KeyError, TypeError, ValueError):
            return MergeStatus.UNHANDLED

    @staticmethod
    def _to_person(data: Dict[Text, Any]) -> Optional[Person]:
        if not data:
            return None
        return Person(data["name"], parse_url(data["web_url"]),
                      parse_url(data["avatar_url"]))

    @property
    @lru_cache()
    def url(self):
        return Url(
            scheme="https", host=self.config.gitlab_hostname,
            path=f"/api/v4/groups/"
                 f"{self.config.gitlab_group}/merge_requests",
            query="per_page=50&state=opened&with_merge_status_recheck=true")

    @property
    @lru_cache()
    def base_url(self):
        return Url(scheme="https", host=self.config.gitlab_hostname)


if __name__ == '__main__':
    basicConfig(level=INFO)
    client = GitlabClient(config=EnvConfigStore())
    client.fetch_projects()
