import json
from threading import Thread
from typing import Dict, Any, Text
from urllib.parse import parse_qs
from urllib.request import Request, urlopen

from slack.errors import SlackApiError
from urllib3.util import Url

from chalicelib.aws import SSMConfigStore
from chalicelib.chalice import SecretiveChalice
from chalicelib.domain import busiest_people
from chalicelib.gitlab import GitlabClient
from chalicelib.slack import (HandledEvent, LazySlackClient, sanitise, format_project,
                              format_people_freqs, Markup)

# Initialise early - we need the decorators
ssm_config_store = SSMConfigStore()
slack_client = LazySlackClient(ssm_config_store)
gitlab_client = GitlabClient(ssm_config_store)
app = SecretiveChalice(app_name="gitlab-branch-bot",
                       slack_client=slack_client, gitlab_client=gitlab_client)


@app.route("/slack/events", methods=["POST"])
def events():
    app.log.debug("Got request: %s", app.current_request.raw_body)
    data = app.current_request.json_body
    if not data:
        raise ValueError("Malformed request - no JSON POST body. "
                         f"Headers: ${app.current_request.headers}")
    try:
        challenge = data["challenge"]
        return {"challenge": challenge}
    except KeyError:
        pass
    try:
        outer_event_type_ = data["type"]
        if outer_event_type_ != "event_callback":
            raise ValueError(f"Unknown event type {outer_event_type_}")
    except KeyError:
        raise ValueError("Missing outer event type from: %s", data)

    event = data.get("event", {})
    raw_event_type = event.get("type", None)
    user_id = event.get("user", None)
    if not user_id:
        # Mentions in reply can cause recursion...
        return ":confused: who are you?"
    try:
        event_type = HandledEvent(raw_event_type)
    except ValueError:
        app.log.warning("Unknown <%s> event from %s: %s", raw_event_type, user_id, data)
        return f":confused: I don't understand `{sanitise(raw_event_type)}` events."
    app.log.info("Got <%s> event from %s: %s", event_type, user_id, data)
    app.log.info("Getting Gitlab MRs")
    question = event.get("text", None)
    text_response = f"> {question}\n\n" if question else ""
    text_response += get_mrs_response(app.gitlab_client.base_url)
    channel = event["channel"]
    try:
        app.slack_client.chat_postMessage(channel=channel, text=text_response)
    except SlackApiError as e:
        app.log.warning("Couldn't post to Slack channel '%s' (%r): %s",
                        channel, e, text_response)
        return {"error": e}
    else:
        app.log.info("Replied to %s's message", user_id or "(unknown user)")
        return {"response": text_response, "reply_to": data["event"]["text"]}


@app.route("/slack/commands", methods=["POST"],
           content_types=['application/x-www-form-urlencoded'])
def commands():
    data = parse_qs(app.current_request.raw_body.decode())
    app.log.info("Got command with data: %s", data)

    def post_response(url: Text, body_data: Dict[Text, Any]):
        req = Request(url, method="POST", headers={"Content-type": "application/json"},
                      data=json.dumps(body_data).encode("utf-8"))
        app.log.info("Posting to %s", url)
        with urlopen(req, timeout=5) as resp:
            if resp.getcode() >= 400:
                app.log.error("Failed posting response to Slack (HTTP %d): %s",
                              resp.getcode, resp.read())

    url = data.get("response_url", [None])[0]
    data = {
        "response_type": "in_channel",
        "delete_original": "true",
        "text": get_mrs_response(app.gitlab_client.base_url)
    }
    # TODO: stronger guarantees this thread will terminate...
    t = Thread(target=post_response, args=(url, data))
    t.run()
    return {
        "response_type": "ephemeral",
        "text": ":information_source: Contacted Gitlab for MR info. Will post back here :eyes:"
    }


def get_mrs_response(base_url: Url) -> Markup:
    try:
        projects_by_id = app.gitlab_client.fetch_projects()
    except Exception as e:
        app.log.warning("Failed getting MRs: %r", e)
        return (f":disappointed: I couldn't reach "
                f"<{app.gitlab_client.url.url}|your Gitlab endpoint> in time. "
                f"Maybe it's me... maybe it's networking? :thinking_face:")
    list_body = "\n".join(format_project(project)
                          for pid, project in projects_by_id.items())

    n = len(projects_by_id)
    projects = projects_by_id.values()
    total = sum(len(p.mrs) for p in projects)
    authors_by_frequency = busiest_people(lambda mr: mr.author, projects)
    assignees_by_frequency = busiest_people(lambda mr: mr.assignee, projects)

    authors_body = format_people_freqs(authors_by_frequency)
    assignees_body = format_people_freqs(assignees_by_frequency)
    return (f":information_source: "
            f"There are *{total} open <{base_url.url}|Gitlab> merge request"
            f"{'' if n == 1 else 's'}*"
            f" across {n} projects:\n"
            f"{list_body}\n\n"
            + (f"Most open MRs: {authors_body}\n" if authors_body else "")
            + (f"Most open reviews: {assignees_body}\n" if assignees_body else ""))
