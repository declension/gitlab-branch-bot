from dataclasses import dataclass
from typing import Text

from hypothesis import given
from hypothesis.provisional import urls
from hypothesis.strategies import (text, data, integers, sets, builds, frozensets,
                                   sampled_from, SearchStrategy)
from urllib3.util import parse_url

from chalicelib.domain import Project, busiest_people, Person


@dataclass(frozen=True)
class MinimalMr:
    title: Text
    author: Person
    assignee: Person


PEOPLE = [Person(name, parse_url(f"https://thebeatles.com/{name}"), None)
          for name in ("John", "Paul", "George", "Ringo")]

generate_mr: SearchStrategy = builds(MinimalMr, text(min_size=4),
                                     sampled_from(PEOPLE), sampled_from(PEOPLE))


def a_project(data, min_mrs=0):
    return Project(id=data.draw(integers(min_value=1000)), name=f"project-{text(min_size=2)}",
                   url=data.draw(urls()),
                   mrs=data.draw(frozensets(min_size=min_mrs, max_size=min_mrs * 3 // 2,
                                            elements=generate_mr)))


@given(data(), integers(min_value=1))
def test_busiest_people(data, max_authors):
    projects = data.draw(sets(min_size=1, elements=builds(lambda: a_project(data))))
    ret = busiest_people(lambda mr: mr.author, projects, max_authors)
    actual_authors = {m.author for p in projects for m in p.mrs}
    assert len(ret) == min(max_authors, len(actual_authors))


@given(data(), integers(min_value=1, max_value=4))
def test_busiest_people_always_with_mr(data, max_authors):
    projects = data.draw(sets(min_size=1, max_size=7,
                              elements=builds(lambda: a_project(data, min_mrs=5))))
    ret = busiest_people(lambda mr: mr.assignee, projects, max_authors)
    assert ret, "No output even with lots of projects..."
    for i, entry in enumerate(ret):
        if i > 0:
            this = entry[1]
            previous = ret[i - 1][1]
            assert this <= previous, f"Entry #{i} is greater than #{i - 1}: {ret}"
    authors = sorted([e[0].name for e in ret])
    assert authors == sorted(list(set(authors))), "Oh dear, duplicated authors"
