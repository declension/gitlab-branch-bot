from unittest.mock import Mock

from pytest import raises
from slack.errors import SlackApiError

from chalicelib.aws import ConfigStore
from chalicelib.slack import LazySlackClient


def test_config_store_access():
    config = Mock(ConfigStore, spec_set=["slack_api_token"])
    client = LazySlackClient(config)
    with raises(SlackApiError) as e:
        client.chat_postMessage(channel="DUMMY_CHANNEL")
    assert "invalid_auth" in str(e)
