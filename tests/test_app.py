import json
from collections import defaultdict
from datetime import datetime
from logging import Logger, INFO
from typing import Any, Dict, Text
from unittest.mock import Mock
from urllib.parse import quote

from _pytest.logging import LogCaptureFixture
from chalice import Chalice
from dateutil.tz import UTC
from pytest import fixture
from slack import WebClient
from urllib3.util import parse_url

from app import app
from chalicelib.chalice import SecretiveChalice
from chalicelib.domain import MergeRequest, MergeStatus, Project, Person
from chalicelib.gitlab import GitlabClient, ProjectsById
from chalicelib.slack import HandledEvent
from tests.config import DummyConfigStore

BARBARA = Person("Barbara Streisand", parse_url("https://www.barbrastreisand.com/"),
                 parse_url("https://barbrastreisand.com"
                           "/wp-content/themes/barbrastreisand/assets/img/encore_home.png"))

AN_MR_URL = parse_url("https://gitlab.example.com/merge_requests/1")

BAD_EVENT_TYPE = "foo"

A_TITLE = "A title"

TEST_MSG = "This is clearly a test?"

A_CMD_BODY = {
    "token": "gIkuvaNzQIHg97ATvDxqgjtO ", "team_id": "T0001 ", "team_domain": "example ",
    "enterprise_id": "E0001 ", "enterprise_name": "Globular%20Construct%20Inc ",
    "channel_id": "C2147483705 ", "channel_name": "test ", "user_id": "U2147483697 ",
    "user_name": "Steve ", "command": "/weather ", "text": "94070 ",
    "response_url": "https://hooks.slack.com/commands/1234/5678 ",
    "trigger_id": "13345224609.738474920.8088930838d88f008e0"}

AN_EVENT_BODY = {
    "type": "event_callback",
    "event": {
        "type": HandledEvent.MESSAGE.value,
        "channel": "1234",
        "text": TEST_MSG,
        "user": "someone"
    }
}

A_BAD_EVENT_BODY = {
    "type": "event_callback",
    "event": {
        "type": BAD_EVENT_TYPE,
        "channel": "1234",
        "text": TEST_MSG,
        "user": "someone"
    }
}


def test_app_basics():
    name = app.app_name.lower()
    assert "gitlab" in name
    assert "branch-bot" in name
    assert app.configure_logs


def test_app_logging(caplog: LogCaptureFixture):
    caplog.set_level(INFO)
    chalice = SecretiveChalice("app", Mock(WebClient), Mock(GitlabClient), configure_logs=False)
    assert isinstance(chalice.log, Logger)
    assert "asctime" not in app.FORMAT_STRING
    chalice.log.info(TEST_MSG)
    record = caplog.get_records("call")[1]
    assert record.levelno == INFO
    assert record.message == TEST_MSG
    lines = caplog.text.splitlines()
    assert len(lines) == 2
    assert "INFO " in lines[1]
    assert TEST_MSG in lines[1]


A_PROJECT_ID = 1234
A_TIME = datetime.now(tz=UTC)

A_MR = MergeRequest(A_TITLE, A_PROJECT_ID,
                    parse_url("https://gitlab.com"),
                    author=BARBARA,
                    assignee=None,
                    created_at=A_TIME, updated_at=A_TIME,
                    merge_status=MergeStatus.CAN_BE_MERGED, wip=False, upvotes=2, downvotes=0)
A_PROJECT = Project(A_PROJECT_ID, "A project", AN_MR_URL, frozenset({A_MR}))
A_PROJECT_ID = "one"


class FakeGitlabClient(GitlabClient):

    def fetch_projects(self) -> ProjectsById:
        return {A_PROJECT_ID: A_PROJECT}


@fixture
def test_app() -> Chalice:
    """Takes the real app (to allow routing), but hacks in stuff"""
    app.config = DummyConfigStore()
    app.slack_client = Mock(WebClient)
    app.gitlab_client = FakeGitlabClient(app.config)
    return app


@fixture
def fake_cmd() -> Dict:
    return cmd_for(A_CMD_BODY)


@fixture
def fake_event() -> Dict:
    return event_for(AN_EVENT_BODY)


@fixture
def bad_event() -> Dict:
    return event_for(A_BAD_EVENT_BODY)


def test_api_call(test_app, fake_event):
    resp = test_app(fake_event, context={})
    assert resp["statusCode"] == 200, f"Failed with {resp['body']}"
    assert A_TITLE in resp["body"]


def test_slash_cmd(test_app, fake_cmd):
    resp = test_app(fake_cmd, context={})
    assert resp["statusCode"] == 200, f"Failed with {resp['body']}"
    assert "Contacted Gitlab" in resp["body"]


def test_invalid_event_type_raises(test_app, bad_event):
    resp = test_app(bad_event, context={})
    assert f"I don't understand `{BAD_EVENT_TYPE}` events" in resp["body"]


def cmd_for(body: Dict[Text, Any]) -> Dict[Text, Any]:
    event = defaultdict(str)
    path = "/slack/commands"
    body = "&".join(f"{quote(k)}={quote(v)}"
                    for k, v in body.items())
    event.update({"requestContext": {"resourcePath": path,
                                     "httpMethod": "POST"},
                  "headers": {'content-type': 'application/x-www-form-urlencoded'},
                  "body": body})
    return event


def event_for(body: Dict[Text, Any]) -> Dict[Text, Any]:
    event = defaultdict(str)
    path = "/slack/events"
    event.update({"requestContext": {"resourcePath": path,
                                     "httpMethod": "POST"},
                  "headers": {'content-type': 'application/json'},
                  "body": json.dumps(body)})
    return event
