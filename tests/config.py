from typing import Any

from chalicelib.aws import ConfigStore


class DummyConfigStore(ConfigStore):

    def __getattr__(self, name: str) -> Any:
        return f"dummy_{name}"
