import json
from datetime import datetime
from pathlib import Path
from typing import Dict
from unittest.mock import Mock

from pytest import fixture

from chalicelib.aws import ConfigStore
from chalicelib.domain import MergeStatus
from chalicelib.gitlab import GitlabClient

REAL_PROJECT_ID = 3


@fixture
def config() -> ConfigStore:
    return Mock(ConfigStore)


@fixture
def client(config) -> GitlabClient:
    return GitlabClient(config)


@fixture
def real_response() -> Dict:
    with open(Path(__file__).parent / "example_mr_response.json") as f:
        return json.load(f)


def test_transform_nothing_produces_nothing(client):
    assert not client._transform_response([])


def test_transform(client, real_response):
    results = client._transform_response(real_response)
    assert results[REAL_PROJECT_ID].name == "my-group/my-project"
    assert results[REAL_PROJECT_ID].url.path == "/my-group/my-project/"


def test_transform_mutable_mrs(client, real_response):
    results = client._transform_response(real_response)
    mrs = results[REAL_PROJECT_ID].mrs
    # this will throw if not mutable...
    assert not mrs.add(list(mrs)[0]), "Doesn't support uniqueness"


def test_unknown_merge_statues(client):
    now = datetime.now().isoformat('T')
    mr = client._to_mr({"merge_status": "checking",
                        "created_at": now, "updated_at": now,
                        "title": "Something", "web_url": "", "project_id": "1234",
                        "author": {"name": "Shakespeare", "web_url": "", "avatar_url": None},
                        "assignee": None, "work_in_progress": True,
                        "upvotes": 1, "downvotes": 0})
    assert mr.merge_status == MergeStatus.UNHANDLED
