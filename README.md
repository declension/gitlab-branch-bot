Gitlab BranchBot
================

[![pipeline status](https://gitlab.com/declension/gitlab-branch-bot/badges/master/pipeline.svg)](https://gitlab.com/declension/gitlab-branch-bot/pipelines)

:new: Supports Gitlab 13.0


What?
-----
It's a Slackbot that talks to (a private) Gitlab 
and posts details on branches and MRs that might need attending.


Why?
----

This is easier than reminding or checking manually.


Usage
-----

Uses Make targets:

### Build
```bash
make build
```

### Deploy
Update your Chalice config in [`config.json`](.chalice/config.json) first, then

```bash
make deploy
```


Development
-----------

### Building
Uses Poetry and virtualenvs for building / packaging:

```bash
poetry install
```

### Testing
```bash
poetry run pytest
```
